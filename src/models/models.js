const { sequelize } = require("../config/db");
const { DataTypes } = require("sequelize");

const User = sequelize.define("user", {
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  name: {
    type: DataTypes.STRING,
    validate: {
      max: 150,
    },
  },
  lastName: {
    type: DataTypes.STRING,
    validate: {
      max: 150,
    },
  },
  email: {
    type: DataTypes.STRING,
    unique: true,
  },
  password: {
    type: DataTypes.STRING,
    unique: true,
  },
  image: {
    type: DataTypes.STRING,
    unique: true,
  },
});

sequelize.sync();

module.exports = User;
