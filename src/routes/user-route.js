const express = require("express");
const router = express.Router();
const { check, body } = require("express-validator");
const { verifyFields } = require("../middlewares");
const {
  createUser,
  findAllUsers,
  updateUser,
  deleteUser,
} = require("../controllers/userController");

router.post(
  "/api/usuarios",
  [
    body(
      ["name", "lastName", "email", "password"],
      "No se permiten elementos vacios"
    ).notEmpty(),
    verifyFields,
  ],
  createUser
);

router.get("/api/usuarios", findAllUsers);

router.put(
  "/api/usuarios/:id",
  [
    body(
      ["name", "lastName", "email", "password"],
      "No se permiten elementos vacios"
    ).notEmpty(),
    verifyFields,
  ],
  updateUser,
);

router.delete(
  "/api/usuarios/:id",
  deleteUser
);

module.exports = router;
