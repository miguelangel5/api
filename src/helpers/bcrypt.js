const { hashSync } = require("bcrypt");

const convertPasswordBcrypt = (password) => {
  let passwordBcrypt = hashSync(password, 10);
  return passwordBcrypt;
};

module.exports = {
  convertPasswordBcrypt,
};
