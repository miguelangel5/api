const { convertPasswordBcrypt } = require("../helpers/bcrypt");
const User = require("../models/models");
const base64toFile = require("node-base64-to-file");
const fs = require("fs");

const createUser = async (req, res) => {
  try {
    const { name, lastName, email, password, image } = req.body;
    let imagePath = "";
    if (image) {
      imagePath = await saveImage(image);
    }
    let responseSend = {
      status: false,
      code: 404,
    };
    if (await validateEmail(email)) {
      responseSend.error = "Error el correo ya se encuentra registrado";
      res.status(responseSend.code).send(responseSend);
    } else {
      const data = User.build({
        name: name,
        lastName: lastName,
        email: email,
        image: imagePath,
        password: convertPasswordBcrypt(password),
      });
      await data.save();
      if (data) {
        responseSend.code = 200;
        responseSend.status = true;
        responseSend.data = data;
      } else {
        responseSend.error = "Ocurrió un error al intentar crear el usuario.";
      }
      res.status(responseSend.code).send(responseSend);
    }
  } catch (error) {
    res.status(404).send(error);
    throw new Error(error);
  }
};

const validateEmail = async (email) => {
  const findAllEmail = await User.findAll({
    where: {
      email: email,
    },
  });
  if (findAllEmail.length > 0) {
    return true;
  } else {
    return false;
  }
};

const findAllUsers = async (req, res) => {
  try {
    let responseSend = {
      status: false,
      code: 404,
    };
    const data = await User.findAll();
    responseSend.code = 200;
    responseSend.status = true;
    responseSend.data = data;
    res.status(responseSend.code).send(responseSend);
  } catch (error) {
    res.status(responseSend.code).send(error);
    throw new Error(error);
  }
};

const deleteUser = async (req, res) => {
  const { id } = req.params;
  let responseSend = {
    status: false,
    code: 404,
  };
  try {
    const data = await User.findByPk(id);
    if (data) {
      if (data.image) {
        await fs.rmSync("./src/uploads/" + data.image, {
          force: true,
        });
      }
      await User.destroy({ where: { id: id } });
      responseSend.code = 200;
      responseSend.status = true;
      responseSend.data = "Usuario eliminado exitosamente";
    }else{
      responseSend.code = 200;
      responseSend.status = true;
      responseSend.data = "El usuario no existe";
    }
    res.status(responseSend.code).send(responseSend);
  } catch (error) {
    res.status(responseSend.code).send(error);
    throw new Error(error);
  }
};

const updateUser = async (req, res) => {
  const { id } = req.params;
  const { name, lastName, email, password, image } = req.body;
  let responseSend = {
    status: false,
    code: 404,
  };
  try {
    let imagePath = "";
    const data = await User.findByPk(id);
    if (data) {
      if (data.image) {
        await fs.rmSync("./src/uploads/" + data.image, {
          force: true,
        });
      }
      imagePath = await saveImage(image);
      (data.id = id),
        (data.name = name),
        (data.lastName = lastName),
        (data.email = email),
        (data.image = imagePath),
        (data.password = convertPasswordBcrypt(password)),
        await data.save();
      responseSend.code = 200;
      responseSend.status = true;
      responseSend.data = data;
    } else {
      responseSend.error = "Error, no se encontro al usuario";
    }
    res.status(responseSend.code).send(responseSend);
  } catch (error) {
    res.status(responseSend.code).send(error);
    throw new Error(error);
  }
};

const saveImage = async (image) => {
  const type = image.split(";")[0].split("/")[1];
  return await base64toFile(image, {
    filePath: "./src/uploads",
    randomizeFileNameLength: 14,
    types: [type],
    fileMaxSize: 3145728,
  });
};

module.exports = {
  createUser,
  findAllUsers,
  deleteUser,
  updateUser,
};
