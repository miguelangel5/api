const { validationResult } = require("express-validator");

const verifyFields = (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({
      status: false,
      error: "La solicitud no fue válida. Los datos enviados no son válidos",
      validations: errors.errors,
      code: 400,
    });
  }

  next();
};

module.exports = {
  verifyFields,
};
