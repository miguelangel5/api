process.env.NODE_ENV = process.env.NODE_ENV || "development";
process.env.HOST = process.env.HOST || "http://localhost";
process.env.PORT = process.env.PORT || 3000;
process.env.TOKEN_SECRET = process.env.TOKEN_SECRET || "secretToken";
process.env.VERSION = "1.0.0";
