const express = require("express");
const apiRoutes = require("./src/routes/routes");
const fileUpload = require("express-fileupload");
const cors = require("cors");

const app = express();
const PORT = 3000;

app.use(cors({ origin: true, credentials: true }));
app.use(express.json({ limit: "25mb" }));
app.use("/api", apiRoutes);
app.use(fileUpload());
app.use(require("./src/routes/routes"));

app.use((req, res, next) => {
  res.status(404);
  const error = new Error(`🔍 - Not Found - ${req.originalUrl}`);
  next(error);
});

app.use((err, req, res, next) => {
  const statusCode = res.statusCode !== 200 ? res.statusCode : 500;
  res.status(statusCode);
  res.json({
    status: false,
    code: statusCode,
    error: err.message,
    ...(process.env.NODE_ENV !== "production"
      ? {
          stack: err.stack,
        }
      : ""),
  });
});

app.listen(PORT, async () => {
  console.log(`Server is running at http://localhost:${PORT}`);
});
